# Consultor de Evaluaciones en Línea - CBTis 96

Sistema minimizado para agregar la capacidad al sitio web educativo CBTis 96 de Vicente Guerrero que permite a los estudiantes y padres de familias consultar las evaluaciones educativas de sus hijos que se encuentran cursando o que estuvieron cursando en años anteriores.

## Paqueterías empleadas

- Material UI
- Axios
- Jest
- Fontsource Roboto
- he
- React-Router-Dom

### Créditos

- Líder del proyecto: Ing. Eliseo Ortega.
- Desarrollador: Mike Rosas. (mikzael@hotmail.com)
