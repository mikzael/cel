import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from 'axios';
import Alert from '@material-ui/lab/Alert';
import { ThemeProvider } from '@material-ui/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import host from '../../host';
import SchoolIcon from '@material-ui/icons/School';




function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
        Si no cuenta con una clave o desconoce el número de control de alumno, favor de contactar a control escolar. <br /><br />
      <Link color="inherit" href="mailto: mikzael@hotmail.com">
        Desarrollado por Mike Rosas
      </Link>{' © '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}


const cbtisTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#4477e3",
      dark: "#4a84fe",
    },
  },
});

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor: '#4477e3',
        '&:hover': {
            backgroundColor: "#4a84fe",
        }  
    },
    icons: {
      fontSize: 60,
    },    
}));



const ConsultaBox = () => {
    const classes = useStyles();

    const [control, setControl] = useState('');
    const [control2, setControl2] = useState('');
    const [clave, setClave] = useState('');
    const [errorMsg, setErrorMsg] = useState('');
    const [emailPass, setEmailPass] = useState(false);
    const [circular, setCircular] = useState(false);
    const [successMsg, setSuccessMsg] = useState('');
    const [parcial, setParcial] = useState('');
    const [historial, setHistorial] = useState('');
    const [nombre, setNombre] = useState('');
    const [numControl, setNumControl] = useState('');
    const [viewLinks, setViewLinks] = useState(false);

    const refreshPage = () => {
      window.location.reload();
    }

    const ShowLinks = () => {

      return (
        <Box style={{"marginTop": "50px", "marginBottom": "50px", "width": "400px"}}>
            <p>No. Control:<br/> <strong>{numControl}</strong></p>
            <p style={{"marginBottom": "80px"}}>Nombre del Alumno:<br/> <strong>{nombre}</strong></p>
            
            {
              parcial ?
                <>
                  <p >Calificaciones Parciales / Finales</p>
                  <p style={{"paddingLeft": "30px"}}><a href={ host + "consulta/pdf/" + parcial } rel="noreferrer" target="_blank">kardex/{control}.pdf</a></p>
                </>
              :
                ""
            }

            {
              historial ?
                <>
                  <p>Historial Académico</p>
                  <p style={{"paddingLeft": "30px"}}><a href={ host + "consulta/pdf/" + historial } rel="noreferrer" target="_blank">historial/{control}.pdf</a></p>
                </>
              :
                ""
            }
          <div style={{textAlign: 'center', marginTop: '40px'}}>
            <Button variant="contained" color="primary" onClick={refreshPage}>
              Salir
            </Button>
          </div>

        </Box>
      );
    }



    const onSubmit = async e => {
      e.preventDefault();
      setCircular(true);

      const formData = new FormData();
      formData.append('control', control);
      formData.append('clave', clave);

      try {
          const res = await axios.post(host + 'consulta', formData, {
              // headers: {
              //     'Content-Type': 'multipart/form-data'
              // },
              use: 'no-cors'
          });

          const { nombre, control, parcial, historial, key } = res.data;

          setNombre(nombre);
          setNumControl(control);
          setParcial(parcial);
          setHistorial(historial);
          setViewLinks(true);

          //window.open(host + "consulta/pdf/" + key, "_blank")
          // console.log(key);

          setErrorMsg('');
          setCircular(false);
      } catch(err) {
          setErrorMsg(err.response.data.error);
          setCircular(false);
          //console.log(err.response.data.error);
      }
  }  

  const sendEmail = async e => {
    e.preventDefault();
    setCircular(true);
    setErrorMsg('');
    setSuccessMsg('')

    const formData = new FormData();
    formData.append('control', control2);

    try {
        const res = await axios.post(host + 'consulta/mail', formData, {
            use: 'no-cors'
        });

      setSuccessMsg(res.data.success)
      setCircular(false);
    } catch(err) {
      setErrorMsg(err.response.data.error);
      setCircular(false);
    }
}  


  return (
    <Container component="main" maxWidth="xs" style={{ marginTop: '150px' }}>
      <CssBaseline />
      <ThemeProvider theme={cbtisTheme}>
      <div className={classes.paper}>
        <Typography component="h1" variant="h4">
          <SchoolIcon className={classes.icons} color="primary" /> CBTis 96
        </Typography>
        <Typography component="h2" variant="h6" style={{ margin: '20px 0px' }}>
          Consultor de Evaluaciones en Línea
        </Typography>

        { circular ? <CircularProgress /> : '' }
        { errorMsg ? <Alert severity="error">{errorMsg}</Alert> : '' }
        { successMsg ? <Alert severity="success">{successMsg}</Alert> : '' }

        { viewLinks ? <ShowLinks /> : '' }

        { !viewLinks ?

            <>

              { emailPass ? 
        
                  <form className={classes.form} noValidate onSubmit={sendEmail}>
                    <TextField
                      variant="outlined"
                      margin="normal"
                      required
                      fullWidth
                      id="control2"
                      label="Número de Control"
                      name="control2"
                      autoFocus
                      onChange={e => setControl2(e.target.value)}
                    />

                    <ThemeProvider theme={cbtisTheme}>
                      <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                      >
                        Solicitar Clave
                      </Button>
                    </ThemeProvider>

                  </form>

                :

                <form className={classes.form} noValidate onSubmit={onSubmit}>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="control"
                    label="Número de Control"
                    name="control"
                    autoFocus
                    onChange={e => setControl(e.target.value)}
                  />
                  <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="clave"
                    label="Clave"
                    type="password"
                    id="clave"
                    onChange={e => setClave(e.target.value)}
                  />

                  
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                    >
                      Consultar
                    </Button>
                </form>

              }

            </>

          :
            ''

        }



      </div>
      </ThemeProvider>

      { !viewLinks ? 

        <>

          { emailPass ? 
            <Box mt={2}>
              <Typography variant="body2" color="textSecondary" align="center">
                <Link color="inherit" onClick={() => setEmailPass(false)} href="#">
                  Consultar una evaluación
                </Link>
              </Typography>
            </Box> 
            :      
            <Box mt={2}>
              <Typography variant="body2" color="textSecondary" align="center">
                <Link color="inherit" onClick={() => setEmailPass(true)} href="#">
                  Enviar clave por correo
                </Link>
              </Typography>
            </Box> 
          }

        </>

        :
          ''
      }


      <Box mt={2}>
        <Copyright />
      </Box>
    </Container>
  );
}

export default ConsultaBox;