import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from 'axios';
import Alert from '@material-ui/lab/Alert';
import LockIcon from '@material-ui/icons/Lock';
import auth from '../../classes/authClass';
import { useHistory } from "react-router";
import { ThemeProvider } from '@material-ui/styles';
import host from '../../host';



function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      <Link color="inherit" href="mailto: mikzael@hotmail.com">
        Desarrollado por Mike Rosas
      </Link>{' © '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const cbtisTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#4477e3",
      dark: "#4a84fe",
    },
  },
});

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2), 
    },
}));



const LoginBox = () => {
    const classes = useStyles();
    const history = useHistory();

    const [user, setUser] = useState('');
    const [pass, setPass] = useState('');
    const [errorMsg, setErrorMsg] = useState('');

    const onSubmit = async e => {
        e.preventDefault();

        const formData = new FormData();
        formData.append('user', user);
        formData.append('pass', pass);

        try {
            const res = await axios.post(host + 'login', formData, { use: 'no-cors' });

            const { key } = res.data;

            auth.setKey(key);
            auth.login(() => {
              history.push("/admin/tablero");
            });

            // console.log(auth.getKey());
            // console.log(key);

            setErrorMsg('');

        } catch(err) {
            setErrorMsg(err.response.data.error);
            //console.log(err);
        }
    }  


  return (
    <Container component="main" maxWidth="xs" style={{ marginTop: '150px' }}>
      <CssBaseline />


      <div className={classes.paper}>
        <Typography component="h1" variant="h4">
          <LockIcon /> Administrador
        </Typography>
        <Typography component="h2" variant="h6" style={{ margin: '20px 0px' }}>
          Consultor de Evaluaciones en Línea
        </Typography>

        { errorMsg ? <Alert severity="error">{errorMsg}</Alert> : '' }

        <form className={classes.form} noValidate onSubmit={ onSubmit }>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="user"
            label="Usuario"
            name="user"
            autoFocus
            onChange={e => setUser(e.target.value)}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="pass"
            label="Contaseña"
            type="password"
            id="pass"
            onChange={e => setPass(e.target.value)}
          />

          <ThemeProvider theme={cbtisTheme}>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Iniciar Sesión
            </Button>
          </ThemeProvider>
        </form>
      </div>

      
      <Box mt={2}>
        <Copyright />
      </Box>
    </Container>
  );
}

export default LoginBox;