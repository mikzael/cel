import Container from '@material-ui/core/Container';
import HeaderBar from "./HeaderBar";
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import UploadBox from './UploadBox';



const cbtisTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#4477e3",
      dark: "#4a84fe",
    }
  },
});




const Integracion = () => {

  return (
  <>
    <HeaderBar />
    <Container style={{textAlign: 'center'}}>
      <ThemeProvider theme={cbtisTheme}>
        <UploadBox />
      </ThemeProvider>
    </Container>
  </>
  );
}

export default Integracion;