import Container from '@material-ui/core/Container';
import HeaderBar from "./HeaderBar";
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { useHistory } from "react-router";
import auth from '../../classes/authClass';
import host from '../../host';

import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import StorageIcon from '@material-ui/icons/Storage';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import GetAppIcon from '@material-ui/icons/GetApp';
import MailIcon from '@material-ui/icons/Mail';


const cbtisTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#4477e3",
      dark: "#4a84fe",
    }
  },
});


const useStyles = makeStyles((theme) => ({
  mainButton: {
    margin: '0 15px 15px 0',
    padding: '0 10px 0 10px',
    minWidth: '200px',
    minHeight: '80px',
  },
  icons: {
    fontSize: 60,
    padding: '0 10px 0 10px',    
  }
}));


const Tablero = props => {
  const classes = useStyles();
  const history = useHistory();

  const menuItems = [
    {
      title: 'Subir',
      pageURL: '/admin/integracion',
      icon: <CloudUploadIcon className={classes.icons} />,
      api: 0,
    },
    {
      title: 'Registros',
      pageURL: '/admin/registros',
      icon: <StorageIcon className={classes.icons} />,
      api: 0,
    },
    {
      title: 'Correos',
      pageURL: '/admin/correos',
      icon: <MailIcon className={classes.icons} />,
      api: 0,
    },
    {
      title: 'Excel',
      pageURL: 'excel/view/' + auth.getKey(),
      icon: <GetAppIcon className={classes.icons} />,
      api: 1,
    },
  ]  

  const handleButtonClick = pageURL  => {
    history.push(pageURL)
  };

  const handleApiClick = pageURL  => {
    window.open(host + pageURL, "_blank")    
  };

  return (
    <>
      <HeaderBar />
      <Container style={{textAlign: 'center', paddingTop: '150px'}}>
        <ThemeProvider theme={cbtisTheme}>

          {
              menuItems.map(menuItem => {
                const {title, pageURL, icon, api} = menuItem;

                return (
                  api === 1 ?
                    <Button 
                        key={title} onClick={() => handleApiClick(pageURL)} 
                        variant="outlined" 
                        color="primary" 
                        disableElevation 
                        className={classes.mainButton}
                    >
                        {icon}
                        <Typography variant="subtitle1" component="span">{title}</Typography>
                    </Button>
                  :
                    <Button 
                        key={title} onClick={() => handleButtonClick(pageURL)} 
                        variant="outlined" 
                        color="primary" 
                        disableElevation 
                        className={classes.mainButton}
                    >
                        {icon}
                        <Typography variant="subtitle1" component="span">{title}</Typography>
                    </Button>
                );
              })
          }

          <Button onClick={() => {
                  auth.logout(() => {
                    history.push("/admin/login");
                  });                    
                }} color="primary" variant="outlined" className={classes.mainButton}>
                  <ExitToAppIcon className={classes.icons} />
                  <Typography variant="subtitle1" component="span">Salir</Typography>
                </Button>

        </ThemeProvider>
      </Container>
    </>
  );
}
  
export default Tablero;