import React from 'react';
import { makeStyles, useTheme, createMuiTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { ThemeProvider } from '@material-ui/styles';
import { useHistory } from "react-router";
import auth from '../../classes/authClass';
import SchoolIcon from '@material-ui/icons/School';
import host from '../../host';

import AppsIcon from '@material-ui/icons/Apps';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import StorageIcon from '@material-ui/icons/Storage';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import GetAppIcon from '@material-ui/icons/GetApp';
import MailIcon from '@material-ui/icons/Mail';


const cbtisTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#4477e3",
      dark: "#4a84fe",
    },
  },
});

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  headerMenuButton: {
    marginLeft: '5px'
  },
}));

const HeaderBar = props => {
  const classes = useStyles();
  const history = useHistory();
  const theme = useTheme();

  const title = "CBTis 96";
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));


  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClick = pageURL  => {
    history.push(pageURL)
    setAnchorEl(null);
  };

  const handleButtonClick = pageURL  => {
    history.push(pageURL)
  };

  const handleApiClick = pageURL  => {
    window.open(host + pageURL, "_blank")    
  };

  const menuItems = [
    {
      title: 'Tablero',
      pageURL: '/admin/tablero',
      icon: <AppsIcon />,
      api: 0
    },
    {
      title: 'Subir',
      pageURL: '/admin/integracion',
      icon: <CloudUploadIcon />,
      api: 0
    },
    {
      title: 'Registros',
      pageURL: '/admin/registros',
      icon: <StorageIcon />,
      api: 0
    },
    {
      title: 'Correos',
      pageURL: '/admin/correos',
      icon: <MailIcon />,
      api: 0
    },
    {
      title: 'Excel',
      pageURL: 'excel/view/' + auth.getKey(),
      icon: <GetAppIcon />,
      api: 1
    },
  ]


  return (
    <div className={classes.root}>

    <ThemeProvider theme={cbtisTheme}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            <SchoolIcon fontSize="large" /> {title}
          </Typography>

          {
            // Ternary que determina si mostrar el appBar en movil o navegador
            isMobile ? 
              <>
                <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={handleMenu}>
                    <MenuIcon />
                </IconButton>
                <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                    }}
                    open={open}
                    onClose={() => setAnchorEl(null)}
                >
                    {
                        menuItems.map(menuItem => {
                          const {title, pageURL} = menuItem;
                          return (
                            <MenuItem key={title} onClick={() => handleMenuClick(pageURL)}>{title}</MenuItem>
                          );
                        })
                    }
                    <MenuItem onClick={() => {
                      auth.logout(() => {
                        history.push("/admin/login");
                      });
                    }}
                    >
                    Salir
                    </MenuItem>
                </Menu>
              </>
            :
              <>
                {
                    menuItems.map(menuItem => {
                      const {title, pageURL, icon, api} = menuItem;
                      return (
                        api === 1 ?
                        <Button 
                          key={title} onClick={() => handleApiClick(pageURL)} 
                          variant="contained" 
                          color="primary" 
                          disableElevation 
                          className={classes.headerMenuButton}
                          startIcon={icon}
                        >{title}</Button>
                      :
                        <Button 
                          key={title} onClick={() => handleButtonClick(pageURL)} 
                          variant="contained" 
                          color="primary" 
                          disableElevation 
                          className={classes.headerMenuButton}
                          startIcon={icon}
                        >{title}</Button>
                      );
                    })
                }
                <Button onClick={() => {
                  auth.logout(() => {
                    history.push("/admin/login");
                  });                    
                }} variant="contained" color="primary" disableElevation className={classes.headerMenuButton} startIcon={<ExitToAppIcon />}>Salir</Button>
              </>
          }

        </Toolbar>
      </AppBar>
      </ThemeProvider>
    </div>
  );
}

export default HeaderBar;
