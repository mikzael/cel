import React, { useState, useEffect } from 'react';
import Container from '@material-ui/core/Container';
import axios from 'axios';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';
import Switch from '@material-ui/core/Switch';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';


import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import DeleteIcon from '@material-ui/icons/Delete';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import host from '../../host';


import HeaderBar from "./HeaderBar";

const cbtisTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#4477e3",
      dark: "#4a84fe",
    },
  },
});


const useStyles = makeStyles({
  container: {
    margin: '60px 0 60px 0',
  },
  table: {
    minWidth: 800,
  },
  thead: {
    fontWeight: 'bold',
  },
  theadRow: {
    backgroundColor: '#eee',
  },
  show: {
    display: 'table-row',
  },
  hide: {
    display: 'none',
  },
  actionButton: {
    minWidth: '32px',
  },
  search: {
    marginLeft: '10px',
  },
  reset: {
    marginLeft: '10px',
    backgroundColor: '#ccc',
    '&:hover': {
      backgroundColor: "#ddd",
    }
  },
});


const Registros = () => {

  const classes = useStyles();

  const [rows, setRows] = useState([]);
  const [tableInfo, setTableInfo] = useState({});
  const [didMount, setDidMount] = useState(false);
  const [page, setPage] = useState(1);
  const [render, setRender] = useState(0);
  const [search, setSearch] = useState('');

  useEffect(() => {

  }, [render]);

  const prevPage = () => {
    return (tableInfo.currentPage > 1 ? (tableInfo.currentPage - 1) : 1);
  }

  const nextPage = () => {
    return (tableInfo.currentPage < tableInfo.totalPages ? (tableInfo.currentPage + 1) : tableInfo.totalPages);
  }


  const tablePager = (rows, currentPage) => {
    const perPage = 10;
    const count = rows.length;
    const totalPages = Math.ceil(count/perPage);

    currentPage = (currentPage > totalPages ? totalPages : currentPage);

    let i;

    const first = perPage * (currentPage - 1);
    let last = (perPage * currentPage);

    if(last > count) last = count;

    // restablece todo a ocultar
    for (i = 0; i < count; i++) {
      rows[i].visibility = 0;
    }

    // solo muestra los que pertenecen a la pagina dada
    for (i = first; i < last; i++) {
      rows[i].visibility = 1;
    }

    tableInfo.count = count;
    tableInfo.currentPage = currentPage;
    tableInfo.first = first;
    tableInfo.last = last;
    tableInfo.totalPages = Math.ceil(count/perPage);
    tableInfo.perPage = perPage;

    setRows(rows);
    setPage(currentPage);
    //console.log(tableInfo);
  }

  useEffect(() => {

  }, [page]);

  const doSearch = async () => {

    const formData = new FormData();
    formData.append('search', search);

    try {
      const res = await axios.post(host + 'registros/search', formData, {
        use: 'no-cors'
      });
      tablePager(res.data.rows, 1);

    } catch(err) {
      console.log(err.response);
    }    
  }

  useEffect(() => {
    const getData = async () => {
      const formData = new FormData();
      // formData.append('pg', 1);

      try {
        const res = await axios.post(host + 'registros/list', formData, {
          use: 'no-cors'
        });
        
        //console.log('render');
        setDidMount(true);
        tablePager(res.data.rows, 1);

      } catch(err) {
        console.log(err.response);
      }
    }

    getData();
  }, []);


  //console.log(rows);
  //console.log('didMount: ' + didMount);


  const toggleStatus = async (id) => {
    const formData = new FormData();
    formData.append('id', id);

    try {
      const res = await axios.post(host + 'registros/toggle', formData, {
        use: 'no-cors'
      });
      
      rows.map((row) => {
        if(row.id === id) row.status = res.data;
      });

      setRender(Math.random() * 1000);

    } catch(err) {
      console.log(err.response);
    }

  };

  const handleDelete = async (row) => {
    if (!window.confirm('Eliminar registro de:\n ' + row.nombre + '?')) return false;

    const formData = new FormData();
    formData.append('id', row.id);

    try {
      const res = await axios.post(host + 'registros/delete', formData, {
        use: 'no-cors'
      });
      
      //console.log(res.data);
      if(res.data.rows.length){
        tablePager(res.data.rows, tableInfo.currentPage);
      }

    } catch(err) {
      console.log(err.response);
    }

  }

  const handleMail = async (row) => {
    const mail = window.prompt("a que correo desea enviar la clave de acceso?");

    if(mail === "" || mail === null) return false;

    // console.log(typeof pass);
    // console.log(pass);

    const formData = new FormData();
    formData.append('control', row.control);
    formData.append('mail', mail);

    try {
      const res = await axios.post(host + 'registros/mail', formData, {
        use: 'no-cors'
      });
      
      //console.log(res.data);
      if(res.data[0] === "ok") window.alert("El correo se ha enviado correctamente.");

    } catch(err) {
      console.log(err.response);
    }


  }

  const handlePassword = async (row) => {
    const pass = window.prompt("Por favor indique una clave nueva para:\n " + row.nombre);

    if(pass === "" || pass === null) return false;

    // console.log(typeof pass);
    // console.log(pass);

    const formData = new FormData();
    formData.append('id', row.id);
    formData.append('pass', pass);

    try {
      const res = await axios.post(host + 'registros/pass', formData, {
        use: 'no-cors'
      });
      
      //console.log(res.data);
      if(res.data[0] === "ok") window.alert("La clave ha sido establecida correctamente.");

    } catch(err) {
      console.log(err.response);
    }
  }



  return (
    <>
      <HeaderBar />
      <Container disableGutters style={{textAlign: 'center'}}>
        <ThemeProvider theme={cbtisTheme}>
          {
            didMount ?



              <TableContainer component={Paper} className={classes.container}>
                <Table className={classes.table} aria-label="simple table">
          
                  <TableHead>
                    <TableRow>
                      <TableCell colSpan={1} align="right"></TableCell>
                      <TableCell colSpan={4}>
                        <TextField
                          variant="outlined"
                          margin="none"
                          size="small"
                          id="search"
                          onChange={ e => {
                            setSearch(e.target.value);
                          }}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <SearchOutlinedIcon />
                              </InputAdornment>
                            ),
                          }}                          
                        />
                        <Button
                          type="button"
                          variant="contained"
                          color="primary"
                          className={classes.search}
                          onClick={doSearch}
                        >
                          Buscar
                        </Button>                                   
                      </TableCell>
                    </TableRow>
                    <TableRow className={classes.theadRow}>
                      <TableCell className={classes.thead} align="center">Estado</TableCell>
                      <TableCell className={classes.thead}>Carrera</TableCell>
                      <TableCell className={classes.thead}>Grupo</TableCell>
                      <TableCell className={classes.thead}>Número de Control</TableCell>
                      <TableCell className={classes.thead}>Nombre</TableCell>
                      <TableCell className={classes.thead}>Parcial</TableCell>
                      <TableCell className={classes.thead}>Historial</TableCell>
                      <TableCell className={classes.thead} align="center">Acciones</TableCell>
                    </TableRow>
                  </TableHead>
          
                  <TableBody>
                    {rows.map((row) => (
                      <TableRow key={row.id} name="pagination" className={ (row.visibility) ? classes.show : classes.hide }>
                        <TableCell component="th" scope="row" style={{width: '80px'}}>
                          <Switch
                            checked={(row.status === 1 ? true : false)}
                            onChange={() => toggleStatus(row.id)}
                            color="primary"
                            name="checkedB"
                            inputProps={{ 'aria-label': 'primary checkbox' }}
                          /> 
                        </TableCell>
                        <TableCell component="th" scope="row">{row.carrera}</TableCell>
                        <TableCell component="th" scope="row">{row.grupo}</TableCell>
                        <TableCell component="th" scope="row">{row.control}</TableCell>
                        <TableCell>{row.nombre}</TableCell>
                        <TableCell><a href={ "http://celapi.cbtis96.edu.mx/kardex/" + row.parcial } rel="noreferrer" target="_blank">{row.parcial}</a></TableCell>
                        <TableCell><a href={ "http://celapi.cbtis96.edu.mx/historial/" + row.historial } rel="noreferrer" target="_blank">{row.historial}</a></TableCell>
                        <TableCell align="center">

                          <IconButton className={classes.actionButton} aria-label="password" size="small" onClick={() => handlePassword(row)} ><VpnKeyIcon /></IconButton>
                          <IconButton className={classes.actionButton} aria-label="mail" size="small" onClick={() => handleMail(row)} ><MailOutlineIcon /></IconButton>
                          <IconButton className={classes.actionButton} aria-label="delete" size="small" onClick={() => handleDelete(row)} ><DeleteIcon /></IconButton>

                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
          
                  <TableBody>
                    <TableRow>
                    <TableCell colSpan={2}>
                        Registros: {tableInfo.count}
                      </TableCell>
                      <TableCell colSpan={3} align="right">
                        Páginas: {tableInfo.currentPage}/{tableInfo.totalPages}

                        <Button 
                          onClick={() => tablePager(rows, prevPage())} 
                          style={{margin: '0 10px'}}
                          variant="outlined" 
                          disableElevation 
                        >
                          <ArrowLeftIcon />
                        </Button>
                        
                        <Button 
                          onClick={() => tablePager(rows, nextPage())} 
                          variant="outlined" 
                          disableElevation 
                        >
                          <ArrowRightIcon />
                        </Button>
                        
                      </TableCell>
                    </TableRow>
                  </TableBody>         
                </Table>
              </TableContainer>


            :
              <CircularProgress style={{marginTop: "150px"}} />
          }
      
        </ThemeProvider>
      </Container>
    </>
  );
}
  
export default Registros;