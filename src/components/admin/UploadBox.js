import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import axios from 'axios';
import Alert from '@material-ui/lab/Alert';
import { ThemeProvider } from '@material-ui/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import host from '../../host';




const cbtisTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#4477e3",
      dark: "#4a84fe",
    },
  },
});

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2), 
    },
    radio: {
        marginTop: theme.spacing(3), 
    },
    icons: {
        fontSize: 60,
    },
    
}));




const LoginBox = () => {
  const classes = useStyles();
  

  const [file, setFile] = useState();
  const [filename, setFilename] = useState('Seleccionar Documento ...');
  const [circular, setCircular] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');
  const [successMsg, setSuccessMsg] = useState('');
  const [radioValue, setRadioValue] = useState('parcial');


  const onChange = e => {
      setFile(e.target.files[0]);
      setFilename(e.target.files[0].name);
  }

  const handleRadioChange = (event) => {
      setRadioValue(event.target.value);
      console.log(event.target.value);
  };  

  const onSubmit = async e => {
      e.preventDefault();

      setCircular(true);
      const formData = new FormData();
      formData.append('file', file);
      formData.append('tipo', radioValue);


      try {
          const res = await axios.post(host + 'upload/new', formData, {
              headers: {
                  'Content-Type': 'multipart/form-data'
              },
              use: 'no-cors'
          });

          //console.log(res.data);
          setSuccessMsg('Documentos PDF encontrados: ' + res.data.pageCount)
          setErrorMsg('');
          setCircular(false);

          //setUploadedFile({ fileName, csvData });
      } catch(err) {
        setSuccessMsg('')
        setErrorMsg(err.response.data.error);
        setCircular(false);
      }

  }




  return (
    <Container component="main" maxWidth="xs" style={{ marginTop: '150px' }}>
      <CssBaseline />
      <ThemeProvider theme={cbtisTheme}>

        <div className={classes.paper}>
            <Box style={{marginBottom: '20px'}}>
                <CloudUploadIcon className={classes.icons} />
                <Typography component="h1" variant="h6">
                    Subir Documento PDF
                </Typography>
            </Box>
            <Box style={{marginBottom: '20px'}}>
                { circular ? <CircularProgress /> : '' }
                { errorMsg ? <Alert severity="error">{errorMsg}</Alert> : '' }
                { successMsg ? <Alert severity="success">{successMsg}</Alert> : '' }
            </Box>

            <form onSubmit={onSubmit}>

              <div className="custom-file">
                  <input type="file" className="custom-file-input" id="customFile" onChange={onChange} />
                  <label className="custom-file-label" htmlFor="customFile">{filename}</label>
              </div>

              <RadioGroup className={classes.radio} aria-label="type" name="type" value={radioValue} onChange={handleRadioChange}>
                  <FormControlLabel value="parcial" control={<Radio checked={ (radioValue === 'parcial')? true : false } color="primary" />} label="Evaluación Parcial" />
                  <FormControlLabel value="historial" control={<Radio checked={ (radioValue === 'historial')? true : false } color="primary" />} label="Historial" />
              </RadioGroup>              

              <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
              >
                  Subir
              </Button>                    
            </form>
        </div>

      </ThemeProvider>
    </Container>
  );
}

export default LoginBox;