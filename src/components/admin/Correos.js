import React, { useState, useEffect } from 'react';
import Container from '@material-ui/core/Container';
import axios from 'axios';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';
import Switch from '@material-ui/core/Switch';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import DeleteIcon from '@material-ui/icons/Delete';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import EditIcon from '@material-ui/icons/Edit';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import host from '../../host';


import HeaderBar from "./HeaderBar";

const cbtisTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#4477e3",
      dark: "#4a84fe",
    },
  },
});


const useStyles = makeStyles({
  container: {
    margin: '60px 0 60px 0',
  },
  table: {
    minWidth: 800,
  },
  thead: {
    fontWeight: 'bold',
  },
  theadRow: {
    backgroundColor: '#eee',
  },
  show: {
    display: 'table-row',
  },
  hide: {
    display: 'none',
  },
  actionButton: {
    minWidth: '32px',
  },
  search: {
    marginLeft: '10px',
  },
  reset: {
    marginLeft: '10px',
    backgroundColor: '#ccc',
    '&:hover': {
      backgroundColor: "#ddd",
    }
  },
});


const Correos = () => {

  const classes = useStyles();

  const [rows, setRows] = useState([]);
  const [tableInfo, setTableInfo] = useState({});
  const [didMount, setDidMount] = useState(false);
  const [page, setPage] = useState(1);
  const [render, setRender] = useState(0);
  const [search, setSearch] = useState('');
  const [open, setOpen] = useState(false);
  const [mail, setMail] = useState('');
  const [mailControl, setMailControl] = useState('');
  const [activeRow, setActiveRow] = useState({});
  const [file, setFile] = useState('');
  const [filename, setFilename] = useState('Documento CSV');
  const [circular, setCircular] = useState(false);
  const [newMsg, setNewMsg] = useState('');


  const handleOpen = (row, index) => {
    row.index = index;
    setActiveRow(row);
    setMail(row.correo);
    setMailControl(row.control);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };



  useEffect(() => {

  }, [render]);

  const prevPage = () => {
    return (tableInfo.currentPage > 1 ? (tableInfo.currentPage - 1) : 1);
  }

  const nextPage = () => {
    return (tableInfo.currentPage < tableInfo.totalPages ? (tableInfo.currentPage + 1) : tableInfo.totalPages);
  }


  const tablePager = (rows, currentPage) => {
    const perPage = 10;
    const count = rows.length;
    const totalPages = Math.ceil(count/perPage);

    currentPage = (currentPage > totalPages ? totalPages : currentPage);

    let i;

    const first = perPage * (currentPage - 1);
    let last = (perPage * currentPage);

    if(last > count) last = count;

    // restablece todo a ocultar
    for (i = 0; i < count; i++) {
      rows[i].visibility = 0;
    }

    // solo muestra los que pertenecen a la pagina dada
    for (i = first; i < last; i++) {
      rows[i].visibility = 1;
    }

    tableInfo.count = count;
    tableInfo.currentPage = currentPage;
    tableInfo.first = first;
    tableInfo.last = last;
    tableInfo.totalPages = Math.ceil(count/perPage);
    tableInfo.perPage = perPage;

    setRows(rows);
    setPage(currentPage);
    //console.log(tableInfo);
  }

  useEffect(() => {

  }, [page]);


  useEffect(() => {
    const getData = async () => {
      const formData = new FormData();
      // formData.append('pg', 1);

      try {
        const res = await axios.post(host + 'correos/list', formData, {
          use: 'no-cors'
        });
        
        //console.log('render');
        setDidMount(true);
        tablePager(res.data.rows, 1);

      } catch(err) {
        console.log(err.response);
      }
    }

    getData();
  }, []);


  //console.log(rows);
  //console.log('didMount: ' + didMount);


  const handleDelete = async (row) => {
    if (!window.confirm('Confirmar que desea eliminar el correo:\n\n No. Control: ' + row.control + '\n Correo: ' + row.correo)) return false;

    const formData = new FormData();
    formData.append('control', row.control);

    try {
      const res = await axios.post(host + 'correos/delete', formData, {
        use: 'no-cors'
      });
      
      //console.log(res.data);
      if(res.data.rows.length){
        tablePager(res.data.rows, tableInfo.currentPage);
      }

    } catch(err) {
      console.log(err.response);
    }

  }


  const handleEdit = async () => {
    
    if(mail === '' || mail === null) return false;
    if(mailControl === '' || mailControl === null) return false;

    // console.log(typeof pass);
    // console.log(pass);

    const formData = new FormData();
    formData.append('control', mailControl);
    formData.append('correo', mail);

    try {
      const res = await axios.post(host + 'correos/update', formData, {
        use: 'no-cors'
      });

      //console.log(res.data);
      //if(res.data[0] === "ok") window.alert("El correo se ha enviado correctamente.");

      rows[activeRow.index].correo = mail;


    } catch(err) {
      console.log(err.response);
    }
    setOpen(false);

  }

  const onSubmit = async e => {

    setCircular(true);
    setNewMsg('subiendo documento');
    setFilename(e.target.files[0].name)
    //console.log(e.target.files[0]);
    //console.log(e.target.files[0].name);

    const formData = new FormData();
    formData.append('file', e.target.files[0]);

    try {
        const res = await axios.post(host + 'correos/new', formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            use: 'no-cors'
        });

        //console.log(res.data);
        setNewMsg('Total encontrados:' + res.data.count + ' | Actualizados:' + res.data.updateCount + ' | Nuevos:' + res.data.newCount);
        setCircular(false);
        setFile('');
        setFilename('Documento CSV');
    } catch(err) {
      console.log(err.response.data.error);
      setNewMsg('');
      setCircular(false);
    }

    try {
      const res = await axios.post(host + 'correos/list', new FormData(), {
        use: 'no-cors'
      });
      
      tablePager(res.data.rows, 1);

    } catch(err) {
      console.log(err.response);
    }    

}



  return (
    <>
      <HeaderBar />
      <Container disableGutters style={{textAlign: 'center'}}>
        <ThemeProvider theme={cbtisTheme}>
          {
            didMount ?
              <TableContainer component={Paper} className={classes.container}>
                <Table className={classes.table} aria-label="simple table">

                  <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Editar Correo</DialogTitle>
                    <DialogContent>
                      <DialogContentText>
                        Indique un correo para el No. Control: {mailControl}
                      </DialogContentText>
                      <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Correo Electrónico"
                        type="email"
                        value={mail}
                        fullWidth
                        onChange={ e => {
                          setMail(e.target.value);
                        }}                        
                      />
                    </DialogContent>
                    <DialogActions>
                      <Button onClick={handleClose} color="primary">
                        Cancel
                      </Button>
                      <Button onClick={handleEdit} variant="contained" color="primary">
                        Editar
                      </Button>
                    </DialogActions>
                  </Dialog>                  
          
                  <TableHead>
                    <TableRow>

                      <TableCell colSpan={3}>

                        <Button
                          variant="contained"
                          component="label"
                          color="primary"
                          id="customFile" 
                          size="small"
                        >
                          <CloudUploadIcon style={{margin: "0 10px 0 0"}} />{filename}
                          <input
                            type="file" 
                            onChange={ e => {
                               onSubmit(e);
                            }}
                            hidden
                          />
                        </Button>
                        { circular ? <CircularProgress size={15} style={{marginLeft: "10px"}} /> : '' }
                        <Typography style={{marginLeft: "10px"}} variant="caption">{newMsg}</Typography>
                                  
                      </TableCell>

                    </TableRow>
                    <TableRow className={classes.theadRow}>
                      <TableCell className={classes.thead}>Número de Control</TableCell>
                      <TableCell className={classes.thead}>Correo</TableCell>
                      <TableCell className={classes.thead} align="center">Acciones</TableCell>
                    </TableRow>
                  </TableHead>
          
                  <TableBody>
                    {rows.map((row, index) => (
                      <TableRow key={row.id} name="pagination" className={ (row.visibility) ? classes.show : classes.hide }>
                        <TableCell component="th" scope="row">{row.control}</TableCell>
                        <TableCell>{row.correo}</TableCell>
                        <TableCell align="center">

                          <IconButton className={classes.actionButton} aria-label="edit" size="small" onClick={() => handleOpen(row, index)} ><EditIcon /></IconButton>
                          <IconButton className={classes.actionButton} aria-label="delete" size="small" onClick={() => handleDelete(row)} ><DeleteIcon /></IconButton>

                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
          
                  <TableBody>
                    <TableRow>
                    <TableCell colSpan={2}>
                        Correos: {tableInfo.count}
                      </TableCell>
                      <TableCell colSpan={3} align="right">
                        Páginas: {tableInfo.currentPage}/{tableInfo.totalPages}

                        <Button 
                          onClick={() => tablePager(rows, prevPage())} 
                          style={{margin: '0 10px'}}
                          variant="outlined" 
                          disableElevation 
                        >
                          <ArrowLeftIcon />
                        </Button>
                        
                        <Button 
                          onClick={() => tablePager(rows, nextPage())} 
                          variant="outlined" 
                          disableElevation 
                        >
                          <ArrowRightIcon />
                        </Button>
                        
                        
                      </TableCell>
                    </TableRow>
                  </TableBody>         
                </Table>
              </TableContainer>


            :
              <CircularProgress style={{marginTop: "150px"}} />
          }
      
        </ThemeProvider>
      </Container>
    </>
  );
}
  
export default Correos;