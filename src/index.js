// Core React Loads
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ProtectedRoute } from './components/admin/ProtectedRoute';

// Route Components
import Consulta from "./components/consulta/Consulta";
import Login from "./components/admin/Login";
import Tablero from "./components/admin/Tablero";
import Integracion from "./components/admin/Integracion";
import Memoria from "./components/admin/Memoria";
import Registros from "./components/admin/Registros";
import Correos from "./components/admin/Correos";


// Roboto Font and Baseline
import '@fontsource/roboto';
import CssBaseline from '@material-ui/core/CssBaseline';

// Other Loads
import './index.css';



// App component
const App = () => {

  return (
    <Router>
      <div className="App">

        <Switch>
          <Route exact path="/" component={Consulta} />
          <Route exact path="/admin" component={Login} />
          <Route exact path="/admin/login" component={Login} />
          <ProtectedRoute exact path="/admin/tablero" component={Tablero} />
          <ProtectedRoute exact path="/admin/integracion" component={Integracion} />
          <ProtectedRoute exact path="/admin/memoria" component={Memoria} />
          <ProtectedRoute exact path="/admin/registros" component={Registros} />
          <ProtectedRoute exact path="/admin/correos" component={Correos} />
          <Route path="*" component={Consulta} />
        </Switch>

      </div>
    </Router>
  );
}


// Main render
ReactDOM.render(
  <>
    <CssBaseline />
    <App />
  </>,
  document.getElementById('root')
);


