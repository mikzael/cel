/**
 * 
 * Clase encargada de realizar el proceso de autentificación con el host remoto.
 * 
 **/

class Auth {

    constructor() {
        this.authenticated = false;
    }

    login(callback) {
        this.authenticated = true;
        callback();
    }

    logout(callback) {
        this.authenticated = false;
        callback();
    }

    isAuthenticated() {
        return this.authenticated;
    }

    setKey(key) {
        this.accesskey = key;
    }

    getKey() {
        return this.accesskey;
    }

}

export default new Auth();